import 'package:flutter/material.dart';
import 'package:pef/net/pes.dart' as pes;


class ImgList extends StatelessWidget{

  const ImgList({super.key});

  @override
  Widget build(BuildContext context) {
    return const ImgListPage(title: 'ImgList');
  }
}

class ImgListPage extends StatefulWidget {
  const ImgListPage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<ImgListPage> createState() => _ImaListState();
}

class _ImaListState extends State<ImgListPage> {

  List<dynamic> imgs = List.empty();

  void _getRankList() async {
    var data = await pes.getRankListTest();
    setState(() {
      imgs = data;
    });
  }


  @override
  void initState() {
    super.initState();
    _getRankList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("hello"),
      ),
      body: ListView(
        children: [
          Column(
            children: [
              Container(
                height: 300,
                child: Image(
                  image: AssetImage('assets/星穹铁道-4.png'),
                  fit: BoxFit.fill,
                ),
              ),
              Container(
                width: 300,
                child:  Image.network(
                  'http://192.168.5.210:8091/file/c/2024/02/15/00/00/116066616_p0.jpg',
                ),
              ),
            ],
          ),
          Column(
            children: List.generate(imgs.length, (index){
              var value2 = imgs[index] as Map<String,dynamic>;
              return Container(
                child: Image.network(
                    "http://192.168.5.210:9095/${value2['thumbnailPath'] as String}"
                ),
              );
            }),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.pop(context);
        },
        child: Text("返回"),
      ),
    );
  }

}
import 'dart:convert';
import 'package:http/http.dart' as http;

/**
 * PEServer服务器请求
 */

Future<List> getRankListTest() async{
  final response = await http.get(Uri.parse("http://192.168.5.210:8090/rank/getRankList"));
  var jsonDecode2 = jsonDecode(response.body) as Map<String, dynamic>;
  print("================body: ${jsonDecode(response.body)}");
  print("================code: ${jsonDecode2["code"] as int}");
  print("================statusCode: ${response.statusCode}");

  var jsonDecode22 = jsonDecode2['data']['list'] as List<dynamic>;
  // for (var value in jsonDecode22) {
  //   var value2 = value as Map<String,dynamic>;
  //   print("thumbnailPath: http://192.168.5.210:9095/${value2['thumbnailPath'] as String}");
  // }

  return jsonDecode22;

}